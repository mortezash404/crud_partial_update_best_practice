﻿using System.Threading.Tasks;
using CrudWebApiApp.Models;
using CrudWebApiApp.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace CrudWebApiApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IPersonManager _personManager;

        public PersonController(IPersonManager personManager)
        {
            _personManager = personManager;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var persons = await _personManager.GetAllAsync();

            if (persons == null)
            {
                return NotFound();
            }

            return Ok(persons);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {

            var person = await _personManager.GetByIdAsync(id);

            if (person == null)
            {
                return NotFound();
            }

            return Ok(person);
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreatePersonDto input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var person = new Person
            {
                Name = input.Name,
                City = input.City
            };

            await _personManager.InsertAsync(person);

            return CreatedAtAction("Post", person);
        }

        [HttpPut]
        public async Task<IActionResult> Put(UpdatePersonDto input)
        {
            var person = new Person
            {
                Id = input.Id,
                City = input.City,
                IsActive = input.IsActive
            };

            await _personManager.UpdateAsync(person);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Put(int id)
        {
            await _personManager.DeleteAsync(id);

            return Ok();
        }
    }
}