﻿namespace CrudWebApiApp.Models
{
    public class UpdatePersonDto
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string City { get; set; }
    }
}