﻿namespace CrudWebApiApp.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string City { get; set; }
    }
}
