﻿using System.ComponentModel.DataAnnotations;

namespace CrudWebApiApp.Models
{
    public class CreatePersonDto
    {
        [Required(ErrorMessage = "پر کردن این فیلد الزامی می باشد")]
        public string Name { get; set; }
        public string City { get; set; }
        
    }
}