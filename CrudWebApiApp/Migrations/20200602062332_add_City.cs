﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrudWebApiApp.Migrations
{
    public partial class add_City : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Person",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "Person");
        }
    }
}
