﻿using CrudWebApiApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrudWebApiApp.Data
{
    public class PersonConfigs : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.ToTable("Person");
            builder.Property(p => p.Name).IsRequired().HasMaxLength(20);
        }
    }
}
