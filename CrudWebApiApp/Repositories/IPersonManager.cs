﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrudWebApiApp.Models;

namespace CrudWebApiApp.Repositories
{
    public interface IPersonManager
    {
        Task<List<Person>> GetAllAsync();
        Task<Person> GetByIdAsync(int id);
        Task InsertAsync(Person person);
        Task UpdateAsync(Person person);
        Task DeleteAsync(int id);
    }
}