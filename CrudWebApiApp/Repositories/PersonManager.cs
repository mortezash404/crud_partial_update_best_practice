﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrudWebApiApp.Data;
using CrudWebApiApp.Models;
using Microsoft.EntityFrameworkCore;

namespace CrudWebApiApp.Repositories
{
    public class PersonManager : IPersonManager
    {
        private readonly AppDbContext _context;

        public PersonManager(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<Person>> GetAllAsync()
        {
            return await _context.Persons.AsNoTracking().ToListAsync();
        }

        public async Task<Person> GetByIdAsync(int id)
        {
            return await _context.Persons.FindAsync(id);
        }

        public async Task InsertAsync(Person person)
        {
            await _context.Persons.AddAsync(person);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Partial Update - فقط دو فیلد بروز می شوند اگر مقدارشان تغییر نکرد همان مقدار قبلی ذخیره خواهد شد
        /// </summary>
        /// <param name="person">مدل ورودی</param>
        /// <returns>هیچی</returns>
        public async Task UpdateAsync(Person person)
        {
            var entry = _context.Entry(person);

            _context.Persons.Attach(person);

            entry.Property(p=>p.IsActive).IsModified = true;
            entry.Property(p=>p.City).IsModified = true;

            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var person = await _context.Persons.FindAsync(id);
            _context.Persons.Remove(person);
            await _context.SaveChangesAsync();
        }
    }
}
